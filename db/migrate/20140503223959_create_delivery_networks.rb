class CreateDeliveryNetworks < ActiveRecord::Migration
  def change
    create_table :delivery_networks do |t|
      t.string :origin
      t.string :destination
      t.decimal :distance

      t.timestamps
    end
  end
end
