function WebServiceController($scope, DeliveryNetwork) {
	
	$scope.calculate_route = function() {
		$scope.routes = DeliveryNetwork.query({
			where: {
				origin: $scope.origin, 
				destination: $scope.destination, 
				litre_value: $scope.litre_value
			}
		});
	}
}