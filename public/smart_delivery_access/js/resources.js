SmartDelivery.factory('restful', ['$resource', '$http', function($resource, $http) {
  return function(url, methodsArg, paramsArg) {

    var params = angular.extend({}, {
      id: '@id',
      format: '.json'
    }, paramsArg);

    var methods = angular.extend({},{});

    var resource = $resource(url + "/:id/:format", params, methods);

    for (var action in resource.prototype) {
      if (!action.indexOf('$')) {
        resource.prototype[action] = function(action, actionParent) {
          return function() {
            this.$action_request = action.substr(1);
            actionParent.apply(this, arguments);
          };
        }(action, resource.prototype[action]);
      }
    }
    return resource;
  };
}]);


SmartDelivery.factory('DeliveryNetwork', ['restful', function(restful) {
  return restful('/delivery_networks/index');
}]);