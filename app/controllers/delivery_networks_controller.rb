class DeliveryNetworksController < ApplicationController
  def index
    origins = []
    destinations = []
    params_from_json = recursive_symbolize_keys! ActiveSupport::JSON.decode(params[:where]) if params[:where]

    if params_from_json
      delivery_network = DeliveryNetwork.find(:all, :conditions => [
        "origin = ? OR destination = ?", 
        params_from_json[:origin], 
        params_from_json[:destination]
      ]).collect{ |dn|
          # if there is a direct route
          if dn.origin == params_from_json[:origin] and dn.destination == params_from_json[:destination]
            direct_route = dn
            break
          elsif dn.origin == params_from_json[:origin]
            origins.push(dn.id)
          elsif dn.destination == params_from_json[:destination]
            destinations.push(dn.id)
          end
      }

      if !direct_route
        origins.each do |o|
          short_origin ||= o.distance
          if o.distance < short_origin
        end
      end

      total_cost = direct_route.distance * params_from_json[:litre_value]
      delivery_network = {

      }

    else
      delivery_network = [{:error => "Origin and destination are required."}]
    end

    respond_to do |format| 
      format.json { render :json => delivery_network }
    end
  end

  def recursive_symbolize_keys!(hash)
    hash.symbolize_keys!
    hash.values.select{|v| v.is_a? Hash}.each{|h| recursive_symbolize_keys!(h)}
    hash
  end
end
